<nav class="navbar navbar-custom navbar-transparent navbar-fixed-top" role="navigation">

		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html">Leaff</a>
			</div>

			<div class="collapse navbar-collapse" id="custom-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="page-scroll"><a href="<?php echo base_url(); ?>#home">Home</a></li>
					<li class="page-scroll"><a href="<?php echo base_url(); ?>#dishes">Dishes</a></li>
					<li class="page-scroll"><a href="<?php echo base_url(); ?>#order">Your Orders</a></li>
					<li class="page-scroll"><a href="<?php echo base_url(); ?>#contact">Contact</a></li>
				</ul>
			</div>

		</div><!-- .container -->

	</nav>