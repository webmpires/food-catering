<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.mycookroom.ru/mywork/Leaff/Leaff-v1.4/Site/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2016 16:49:19 GMT -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Leaff Restaurant Theme</title>

	<!-- CSS -->
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

	<!-- Font Awesome CSS -->
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

	<!-- Elegant icons CSS -->
	<link href="<?php echo base_url(); ?>assets/css/simple-line-icons.css" rel="stylesheet" media="screen">

	<!--[if lte IE 7]>
		<script src="<?php echo base_url(); ?>assets/js/icons-lte-ie7.js"></script>
	<![endif]-->

	<!-- Magnific-popup lightbox -->
	<link href="<?php echo base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet">

	<!-- Owl Carousel -->
	<link href="<?php echo base_url(); ?>assets/css/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">

	<!-- Animate css -->
	<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">

	<!-- Time and Date piker CSS -->
	<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet" media="screen">

	<!-- Custom styles CSS -->
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen">
</head>
<body>

	<!-- Preloader -->

	<div id="preloader">
		<div id="status">
			<div class="status-mes"></div>
		</div>
	</div>

	<!-- Navigation start -->

	<?php 
		$this->load->view('includes/nav');
	?>
	<!-- Navigation end -->

	<!-- Home start -->

	<section id="home">

	</section>

	<!-- Home end -->

	<!-- Dishes start -->

	<section id="dishes" class="module">
		<div class="container">

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="module-header wow fadeInUp">
						<h2 class="module-title">Book Now</h2>
					</div>
				</div>
			</div><!-- .row -->

			<div class="row">

				<div class="col-sm-4">
					<div class="menu-classic">
						<figure class="overlay">
							<img src="<?php echo base_url(); ?>assets/images/dishes-1-1.jpg" alt="">
							<figcaption>
								<a href="assets/images/dishes-1-1.jpg" class="popup-image" title="Wild Mushroom Bucatini"></a>
								<div class="caption-inner">
									<div class="overlay-icon">
										<span class="icon-magnifier"></span>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="menu">
							<div class="row">
								<div class="col-sm-8">
									<h3 class="menu-title2"><a style="color:#000" href="<?php base_url(); ?>index.php/book/catering">Catering</a></h3>
								</div>
								<div class="col-sm-4 menu-price-detail">
									<h3 class="menu-price2">&#8377;100/dish</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="menu-classic">
						<figure class="overlay">
							<img src="<?php echo base_url(); ?>assets/images/dishes-2-2.jpg" alt="">
							<figcaption>
								<a href="assets/images/dishes-2-2.jpg" class="popup-image" title="Imported Oysters Grill"></a>
								<div class="caption-inner">
									<div class="overlay-icon">
										<span class="icon-magnifier"></span>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="menu">
							<div class="row">
								<div class="col-sm-8">
									<h3 class="menu-title2"><a style="color:#000" href="<?php base_url(); ?>index.php/book/rajbhog">Rajbhog</a></h3>
								</div>
								<div class="col-sm-4 menu-price-detail">
									<h3 class="menu-price2">&#8377;150/dish</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="menu-classic">
						<figure class="overlay">
							<img src="<?php echo base_url(); ?>assets/images/dishes-3-3.jpg" alt="">
							<figcaption>
								<a href="assets/images/dishes-3-3.jpg" class="popup-image" title="Lemon and Garlic Green Beans"></a>
								<div class="caption-inner">
									<div class="overlay-icon">
										<span class="icon-magnifier"></span>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="menu">
							<div class="row">
								<div class="col-sm-8">
									<h3 class="menu-title2"><a style="color:#000" href="<?php base_url(); ?>index.php/book/dhanumas">Dhanumas</a></h3>
								</div>
								<div class="col-sm-4 menu-price-detail">
									<h3 class="menu-price2">&#8377;150/dish</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div><!-- .row -->

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="devider">
						<img src="<?php echo base_url(); ?>assets/images/divider-down.svg" alt="">
					</div>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</section>

	<!-- Dishes end -->

	<!-- Footer start -->

	<footer id="footer">
		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<p class="copyright">
						© 2015 <a href="#">Leaff</a>, All Rights Reserved.
					</p>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</footer>

	<!-- Footer end -->

	<!-- Scroll-up -->

	<div class="scroll-up">
		<a href="#totop"><i class="fa fa-angle-double-up"></i></a>
	</div>

	<!-- Javascript files -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

	<!-- Use for slideshow background -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>

	<!-- Use for video background -->
	<script src="<?php echo base_url(); ?>assets/js/video.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bigvideo.js"></script>

	<!-- Date and time picker -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.datepair.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jqBootstrapValidation.js"></script>

	<!-- Google Maps -->
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url(); ?>assets/js/gmaps.js"></script>

	<!-- Other -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.matchHeight-min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/twitterFetcher_min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

	<!-- Custom scripts -->
	<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>

<!-- Mirrored from www.mycookroom.ru/mywork/Leaff/Leaff-v1.4/Site/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2016 16:50:43 GMT -->
</html>