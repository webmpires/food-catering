<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.mycookroom.ru/mywork/Leaff/Leaff-v1.4/Site/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2016 16:49:19 GMT -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Leaff Restaurant Theme</title>

	<!-- CSS -->
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

	<!-- Font Awesome CSS -->
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

	<!-- Elegant icons CSS -->
	<link href="<?php echo base_url(); ?>assets/css/simple-line-icons.css" rel="stylesheet" media="screen">

	<!--[if lte IE 7]>
		<script src="<?php echo base_url(); ?>assets/js/icons-lte-ie7.js"></script>
	<![endif]-->

	<!-- Magnific-popup lightbox -->
	<link href="<?php echo base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet">

	<!-- Owl Carousel -->
	<link href="<?php echo base_url(); ?>assets/css/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">

	<!-- Animate css -->
	<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">

	<!-- Time and Date piker CSS -->
	<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet" media="screen">

	<!-- Custom styles CSS -->
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen">
</head>

<body>
	<!-- Preloader -->

	<div id="preloader">
		<div id="status">
			<div class="status-mes"></div>
		</div>
	</div>

	<!-- Navigation start -->

	<?php 
		$this->load->view('includes/nav');
	?>
	<!-- Navigation end -->
	<!-- Popular start -->

	<section id="popular" class="module">
		<div class="container">

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="module-header wow fadeInUp">
						<h2 class="module-title">Dhanumas</h2>
						<a href="#book" class="btn btn-success">Book Now</a>
						<h3 class="module-subtitle">Our menu</h3>
					</div>
				</div>
			</div><!-- .row -->

			<div class="row">

				<div class="col-sm-6">

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Wild Mushroom Bucatini with Kale</h4>
								<div class="menu-detail">Mushroom / Veggie / White Sources</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$10.5</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Lemon and Garlic Green Beans</h4>
								<div class="menu-detail">Lemon / Garlic / Beans</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$14.5</h4>
								<div class="menu-label">New</div>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">LambBeef Kofka Skewers with Tzatziki</h4>
								<div class="menu-detail">Lamb / Wine / Butter</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$18.5</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Imported Oysters Grill (5 Pieces)</h4>
								<div class="menu-detail">Oysters / Veggie / Ginger</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$15.9</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Meatloaf with Black Pepper-Honey BBQ</h4>
								<div class="menu-detail">Pepper / Chicken / Honey</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$16.4</h4>
							</div>
						</div>
					</div>

				</div><!-- .col-sm-6 -->

				<div class="col-sm-6">

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Wild Mushroom Bucatini with Kale</h4>
								<div class="menu-detail">Mushroom / Veggie / White Sources</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$14.5</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Lemon and Garlic Green Beans</h4>
								<div class="menu-detail">Lemon / Garlic / Beans</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$14.5</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">LambBeef Kofka Skewers with Tzatziki</h4>
								<div class="menu-detail">Lamb / Wine / Butter</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$18.5</h4>
								<div class="menu-label">Recommended</div>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Imported Oysters Grill (5 Pieces)</h4>
								<div class="menu-detail">Oysters / Veggie / Ginger</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$15.9</h4>
							</div>
						</div>
					</div>

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">Meatloaf with Black Pepper-Honey BBQ</h4>
								<div class="menu-detail">Pepper / Chicken / Honey</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">$16.4</h4>
							</div>
						</div>
					</div>

				</div><!-- .col-sm-6 -->

			</div><!-- .row -->

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 long-up text-center">
					<a href="menu-simple.html" class="btn btn-custom-1">View Menu</a>
				</div>
			</div><!-- .row -->

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="devider">
						<img src="<?php echo base_url(); ?>assets/images/divider-down.svg" alt="">
					</div>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</section>

	<!-- Popular end -->
	<section id="book" class="module">
		<div class="container">
			<h3>Fill Details</h3>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5">
		  			<form>
		 			<div class="form-group">
		  			<label for="name">Name:</label>
		  			<input type="text" class="form-control" name="name" placeholder="Enter name" maxlength="100">
		  			</div>
		  			<div class="form-group">
      					<label for="Number">Phone Number:</label>
      					<input type="number" class="form-control" name="phone_number" placeholder="Phone Number" min="10" max="11">
    				</div>
    				<div class="form-group">
      					<label for="email">Email:</label>
      					<input type="email" class="form-control" name="email" placeholder="Enter email" maxlength="150">
    				</div>
    				<div class="form-group">
      					<label for="person">Person:</label>
      					<input type="number" class="form-control" name="number_of_person" placeholder="Number Of Person" min="2" max="6">
    				</div>
    				<div class="form-group">
      					<label for="Date">Date:</label>
      					<input type="date" class="form-control">
    				</div>
    				<div class="form-group">
      					<label for="place">Place:</label>
      					<input type="text" class="form-control" name="place" maxlength="90" placeholder="place">
    				</div>
    				<button type="submit" class="btn btn-default">Submit</button>
  					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer start -->

	<footer id="footer">
		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<p class="copyright">
						© 2015 <a href="#">Leaff</a>, All Rights Reserved.
					</p>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</footer>

	<!-- Footer end -->

	<!-- Scroll-up -->

	<div class="scroll-up">
		<a href="#totop"><i class="fa fa-angle-double-up"></i></a>
	</div>

	<!-- Javascript files -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

	<!-- Use for slideshow background -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>

	<!-- Use for video background -->
	<script src="<?php echo base_url(); ?>assets/js/video.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bigvideo.js"></script>

	<!-- Date and time picker -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.datepair.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jqBootstrapValidation.js"></script>

	<!-- Google Maps -->
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url(); ?>assets/js/gmaps.js"></script>

	<!-- Other -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.matchHeight-min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/twitterFetcher_min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

	<!-- Custom scripts -->
	<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>	
</html>